package com.example.lab2.services;

import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.lab2.DTO.ProductDTO;
import com.example.lab2.models.ProductModel;
import com.example.lab2.repository.ProductRepository;

@Service
public class ProductService {

    @Autowired
    private ProductRepository repository;

    public ProductModel updateProduct(int id, ProductDTO productUpdated){
        ProductModel updateProduct = repository.findById(id).orElseThrow(()->new EntityNotFoundException("Música não encontrada, verifique se o ID está correto: " +id));

        if(productUpdated.titulo() != null) updateProduct.setTitulo(productUpdated.titulo());
        if(productUpdated.genero() != null) updateProduct.setGenero(productUpdated.genero());
        if(productUpdated.artista() != null) updateProduct.setArtista(productUpdated.artista());
        if(productUpdated.duracao() > 0) updateProduct.setDuracao(productUpdated.duracao());

        return repository.save(updateProduct);
    }
}
