package com.example.lab2.models;

import com.example.lab2.DTO.ProductDTO;
import jakarta.persistence.*;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="musicas")
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@NoArgsConstructor
public class ProductModel {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;
    private String titulo;
    private String genero;
    private String artista;
    private float duracao;



    public ProductModel(ProductDTO productDTO){
        this.titulo = productDTO.titulo();
        this.genero = productDTO.genero();
        this.artista = productDTO.artista();
        this.duracao = productDTO.duracao();

    }
}
