package com.example.lab2.controller;

import com.example.lab2.models.ProductModel;
import com.example.lab2.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.example.lab2.DTO.ProductDTO;
import com.example.lab2.services.ProductService;

import java.util.List;


@RequestMapping("/")
@RestController
public class ControllerAPI {


    @Autowired
    private ProductService productService;
    private final ProductRepository productRepository;

    @Autowired
    public ControllerAPI(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    //cria um produto no bd
    @PostMapping
    public ResponseEntity<ProductModel> createProduct(@RequestBody ProductDTO productDTO){
        ProductModel productModel1 = new ProductModel(productDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(productRepository.save(productModel1));
    }

    //exibe os produtos presentes no bd
    @GetMapping
    public ResponseEntity<List<ProductModel>> getProducts(){
        return ResponseEntity.ok(productRepository.findAll());
    }

    //deleta um produto do bd pelo id
    @DeleteMapping("/{id}")
    public ResponseEntity<ProductModel> deleteProduct(@PathVariable int id){
        productRepository.deleteById(id);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    //atualiza um produto no bd pelo id
    @PutMapping("/{id}")
    public ResponseEntity<ProductModel> changeProduct(@PathVariable int id, @RequestBody ProductDTO productDTO){
        var product = productService.updateProduct(id, productDTO);
        return ResponseEntity.status(HttpStatus.OK).body(product);
    }

    @GetMapping("/name/{titulo}")
    public ResponseEntity<ProductModel> searchByTitulo(@PathVariable String titulo){
        var product = productRepository.findByTitulo(titulo).orElseThrow();
        return ResponseEntity.ok(product);
    }

    @GetMapping("/music/{genero}")
    public ResponseEntity<ProductModel> searchByGenero(@PathVariable String genero){
        var product = productRepository.findByGenero(genero).orElseThrow();
        return ResponseEntity.ok(product);
    }

    @GetMapping("/artist/{artista}")
    public ResponseEntity<ProductModel> searchByArtista(@PathVariable String artista){
        var product = productRepository.findByArtista(artista).orElseThrow(() -> new RuntimeException("Artista não encontrado"));
        return ResponseEntity.ok(product);
    }
}
