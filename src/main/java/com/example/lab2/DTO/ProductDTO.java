package com.example.lab2.DTO;

public record ProductDTO(String titulo, String genero, String artista, float duracao) {
}
