# Readme - Projeto de Gerenciamento de Produtos

Este é um projeto de API RESTful desenvolvido para gerenciar músicas em um banco de dados. A API oferece operações básicas de criação, leitura, atualização e exclusão (CRUD) de músicas, bem como métodos para pesquisar músicas por  titulo, gênero e artista.

## Tecnologias Utilizadas

- **Spring Boot**: Framework utilizado para o desenvolvimento da aplicação.
- **Java**: Linguagem de programação principal do projeto.
- **Spring Data JPA**: Biblioteca para acesso a dados com Java Persistence API.
- **ElephantSQL Database**: Banco de dados online postgresql.
- **Maven**: Gerenciador de dependências e construção do projeto.

## Funcionalidades da API

- **Criação de Música**:  Permite adicionar uma nova música ao banco de dados.
- **Listagem de música**: Retorna uma lista de todas as músicas armazenados no banco de dados.
- **Exclusão de música**: Remove uma música do banco de dados com base no seu ID.
- **Atualização de música**: Atualiza os dados de uma música existente no banco de dados.
- **Pesquisa por titulo**: Permite buscar uma música pelo seu titulo.
- **Pesquisa por artista**: Permite buscar uma musica pelo seu artista.
- **Pesquisa por gênero**: Permite buscar uma musica pelo seu gênero.

## **Endpoints da API**

- **POST /**: Cria uma nova música no banco de dados.
- **GET /**: Retorna todas as músicas armazenados.
- **DELETE /{id}**: Remove uma música do banco de dados com base no seu ID.
- **PUT /{id}**: Atualiza os dados de uma música existente no banco de dados.
- **GET /name/{titulo}**: Busca uma música pelo seu titulo.
- **GET /music/{genero}**: Busca uma música pelo seu gênero.
- **GET /artist/{artista}**: Busca uma música pelo seu artista.

## **Uso da Classe ControllerAPI**

A classe `ControllerAPI` é responsável por definir os endpoints da API e os métodos que executam as operações sobre os produtos no banco de dados. Abaixo estão detalhadas as principais funcionalidades implementadas:

- `createProduct`: Cria uma nova música no banco de dados com base nos dados fornecidos no corpo da requisição.
- `getProducts`: Retorna todas as músicas armazenadas no banco de dados.
- `deleteProduct`: Remove uma música do banco de dados com base no ID fornecido na URL.
- `changeProduct`: Atualiza os dados de uma música existente no banco de dados com base no ID fornecido na URL e nos dados fornecidos no corpo da requisição.
- `searchByGenero`: Busca um música pelo seu gênero.
- `searchByArtista`: Busca uma música pelo seu Artista.

Certifique-se de seguir corretamente a estrutura dos endpoints e os tipos de dados esperados para uma integração adequada com a API.

## Execução do Projeto

Para executar o projeto localmente, siga estas etapas:

1. Certifique-se de ter o Java JDK e o Maven instalados em seu sistema.
2. Clone este repositório em sua máquina local.
3. Navegue até o diretório raiz do projeto.
4. Execute o comando `mvn spring-boot:run` no terminal para iniciar o servidor.
5. Acesse os endpoints da API usando um cliente REST como Postman ou cURL.

